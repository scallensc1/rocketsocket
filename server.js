import { dirname } from 'path';
import { fileURLToPath } from 'url';
import { instrument } from '@socket.io/admin-ui';
import { Server } from "socket.io";
import chalk from 'chalk'
import consoleStamp from 'console-stamp'
import express from 'express';
import path from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

import { wsClient } from './clients/wsClient.js';
import { ioClient } from './clients/ioClient.js';

// Set logging format with console-stamp
consoleStamp(console, { pattern: 'dd/mm/yyyy HH:MM:ss' });
console.log(ioClient.active ? chalk.blueBright('Node socket.io client initialised') : chalk.redBright('Node socket.io client error'))

// Instantiate express
const app = express();

// Define constants
const PORT = 6969
const GAMEHOST = { type: 'GAME', url: 'http://localhost:49122' }
const RCONHOST = { type: 'RCON', url: 'http://localhost:9002' }

// Use express middleware
app.use(express.json());

// Allow CORS
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  next();
});

// Routes
app.use('/', express.static(path.join(__dirname, './dist/client'))); // use dist/client for overlay
app.use('/socket', express.static(path.join(__dirname, './dist/socketadmin'))); // socket.io admin panel

// Kill server
process.on('SIGINT', async function () {
  console.log(chalk.yellow('Server shutting down'));
  process.exit();
});

// Initialise server
const server = app.listen(PORT, () => {
  console.log(`${chalk.yellowBright("Express/socket.io server")} listening at ${chalk.blue(`http://localhost:${PORT}`)}`);
  console.log(chalk.greenBright(`Ready...`));
});

// Init socket.io server, pass express app through to io constructor
const io = new Server(server, {
  cors: { origin: '*' }
})

// Enable socket.io admin panel access
instrument(io, { auth: false });

// Create client socket instances
wsClient(GAMEHOST, io); // Instantiate ws connection to SOS
const rconClient = wsClient(RCONHOST, io); // Instantiate ws connection to SOS RCON as variable to allow access to `.send` method on class

// Init empty clients object for storing friendly client names
const clients = {}

// Socket logic for client connections
io.on('connection', (socket) => {
  socket.on('join', (clientName) => {
    clients[socket.id] = clientName;
    socket.join(clientName)
    console.log(`[${clientName}] connected`)
  });

  // Place clients into 'game' room to receive SOS messages
  socket.on('watchGame', () => {
    socket.join('game');
    console.log(`[${clients[socket.id]}] in rooms: ${Array.from(socket.rooms).join(', ')}`);
  });

  // Delete client from clients object on disconnection
  socket.on('disconnect', () => {
    console.log(`[${clients[socket.id]}] disconnected`);
    delete clients[socket.id];
  });

  // Emit payload data to clients
  socket.on('payload', (payload) => {
    console.log(payload);
    socket.to('game').emit('payload', payload);
  });

  // Emit RCON data to clients
  socket.on('RCON', (payload) => {
    console.log(`${chalk.blueBright('RCON command:')} ${chalk.yellowBright(payload.data.command)}`)
    rconClient.send(`${payload.data.command}`);
    io.emit('RCON', payload)
  });

  socket.on('error', (err) => {
    console.error(chalk.red(err.message));
  });
});

