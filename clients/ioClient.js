import io from 'socket.io-client'

// Teams array, store colours, team names, score, etc.
let teams = [{}, {}];
teams[0].color = [0, 0, 255, 255];
teams[0].name = '';
teams[0].score = 0;
teams[1].color = [255, 35, 0, 255];
teams[1].name = '';
teams[1].score = 0;

// Player data
let players = {};

// Connect back to the SOS relay on this server to receive Rocket League game data, etc.
export const ioClient = io(`ws://localhost:6969`);
ioClient.emit('join', 'NODE-BACKEND');
ioClient.emit('watchGame');

// Logic for each game tick update event
ioClient.on('update', (data) => {
  let event = data.event;
  let stats = data.data;

  /* Logic for 'game:update_state' events, this will contain all the game data, such as time, team numbers/names, player data,
  score, statistics, etc etc. */
  if (event == 'game:update_state') {
    players = stats['players'];
    teams[0].color = stats.game.teams[0]?.color_primary;
    teams[0].name = stats.game.teams[0].name;
    teams[0].score = stats.game.teams[0].score;
    teams[1].color = stats.game.teams[0]?.color_primary;
    teams[1].name = stats.game.teams[1].name;
    teams[1].score = stats.game.teams[1].score;
  }

  // do things with goal scored events
  if (event == 'game:goal_scored') {
    console.log(stats);
    // put code here
  }

  // do things with match ended/created event
  if (
    event == 'game:match_destroyed' ||
    event == 'game:match_ended' ||
    event == 'game:match_created'
  ) {
    // put code here
  }

  // do things with statfeed events
  if (event == 'game:statfeed_event') {
    console.log(stats);
    // put code here
  }
});