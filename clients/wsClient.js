import WebSocket from 'ws';
import appdata from 'appdata-path';
import fs from 'fs';
import chalk from 'chalk';

// Read rcon_password from bakkes dir
const getRCONPass = () => {
  try {
    const data = fs.readFileSync(appdata.getAppDataPath('\\bakkesmod\\bakkesmod\\cfg\\config.cfg'), 'utf8');
    const file = data.split('\n');
    const index = file.findIndex((element) => element.startsWith("rcon_password"));
    return file[index].split("\"")[1];
  } catch (err) {
    console.log(chalk.redBright(err));
    return null;
  }
}

// Define new websocket class to allow automatic reconnection, etc.
class ReconnectingWebSocket {
  constructor(url, io, RCONPASS) {
    this.url = url;
    this.io = io;
    this.RCONPASS = RCONPASS;
    this.ws = null;
    this.attempts = 0;
    if (this.RCONPASS) {
      console.log(`RCONPASS loaded: ${RCONPASS}`)
    }
    this.connect();
  }

  // Connect logic
  connect() {
    // Connect websocket at given HOST.url
    this.ws = new WebSocket(this.url);

    // If instantiated as an RCON client, emit relevant messages
    this.ws.on('open', () => {
      this.attempts = 0;
      if (this.RCONPASS) {
        console.log(`Connected to ${chalk.yellow('RCON')} on ${chalk.blue(this.url)}`);
        console.log(`Sending ${chalk.yellow("rcon_password")}`)
        this.ws.send(`rcon_password ${this.RCONPASS}`);
        console.log(`Sending ${chalk.yellow("rcon_refresh_allowed")}`)
        this.ws.send('rcon_refresh_allowed');
      } else {
        console.log(`Connected to ${chalk.yellow('Rocket League')} on ${chalk.blue(this.url)}`);
      }
    });

    // 'message' handler
    this.ws.on('message', (message) => {
      if (message == 'authyes') {
        console.log(chalk.greenBright(`RCON authorised`))
      } else { // Parse all other messages and emit to the 'game' room
        let data = JSON.parse(message);
        this.io.in('game').emit('update', data);
      }
    });

    // Close socket on error
    this.ws.on('error', (err) => {
      console.error(chalk.red('Error connecting to RCON!'));
      this.close();
    });

    // When '.close' method is invoked, trigger reconnect attempt
    this.ws.on('close', (code) => {
      console.log('RCON WebSocket Server Closed!');
      console.log('Attempting reconnection...');
      this.reconnect(code);
    });
  }

  // Reconnect logic
  reconnect() {
    setTimeout(() => this.connect(), 5000);
  }

  // Close logic
  close() {
    this.ws.close();
  }

  // Send messages to socket
  send(message) {
    if (this.ws && this.ws.readyState === WebSocket.OPEN) {
      this.ws.send(message);
    } else {
      console.error('WebSocket is not open: cannot send message');
    }
  }
}

// Export function - getRCONPass logic is triggered with relevant HOST.type being passed when instantiated
export function wsClient(HOST, io) {
  let RCONPASS
  if (HOST.type === 'RCON') { RCONPASS = getRCONPass() };
  return new ReconnectingWebSocket(HOST.url, io, RCONPASS);
}

