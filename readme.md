# Rocket League `socket.io` relay server

Connects to SOS / RCON sockets in BakkesMod plugin as a relay, providing RCON functionality.

**_node v14+ required_**

- Clone the repo, then run 'npm install'

- Start server with 'npm run start'

# Client usage

### HTML
check https://cdnjs.com/libraries/socket.io for current links, then import via script tag:

```html
<head>
  <!-- other script tags, etc -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/VERSION.NUMBER.HERE/socket.io.min.js"></script>
  <!-- other script tags, etc -->
</head>

<body>
  <script>
    const socket = io('http://localhost:6969');
    socket.on('connect', () => {
      // emit join message to socket with client ID
      socket.emit('join', 'FRONTEND_CLIENT_NAME_HERE');
      // emit watchGame message to join the 'game' room and receive socket messages
      socket.emit('watchGame');
    });
    // handle data from 'update' message from SOS websocket
    socket.on('update', (update) => {
      // run socket logic here, e.g:
      if (update.event === 'game:update_state') {
        //do stuff with update
        console.log(update.data);
        //etc
      }
    });
  </script>
</body>
```

### Frameworks - React/Vue/Svelte, etc.

```js
import socketIOClient from 'socket.io-client';

const socket = socketIOClient('http://localhost:6969');
socket.on('connect', () => {
  // emit join message to socket with client ID
  socket.emit('join', 'FRONTEND_CLIENT_NAME_HERE');
  // emit watchGame message to join the 'game' room and receive socket messages
  socket.emit('watchGame');
})
// handle data from 'update' message from SOS websocket
socket.on('update', (update) => {
  // run socket logic here, e.g:
  if (update.event === 'game:update_state') {
    // do stuff with update
    console.log(update.data)
    //etc
  }
})
```

# ButtonMash

ButtonMash plugin can be found in the `ButtonMash` folder, this plugin has several functions, allowing automatic join as spectator when launching a game, automatic hiding of HUD elements, and allowing `0-9` keys to be sent via RCON, to allow changing camera angles from an overlay control panel.  

## Installation
From `BakkesMod` launcher, `File -> Open BakkesMod folder`  
Copy `ButtonMash.dll` to the `plugins` folder   
Inside the `cfg` folder, edit `plugins.cfg`, adding buttonmash to the plugin list with `plugin load buttonmash`  

## RCON
In order to utilise the RCON functionality for camera switching   

From BakkesMod launcher, `File -> Open BakkesMod folder`  
navigate to the `data` folder  
Edit the `rcon_commands.cfg` file  
Ensure the following commands are present:  
`rcon_refresh_allowed`  
`replay_gui`  
`ButtonMash`  

## Usage
With `BakkesMod` running, launch `Rocket League`  
Press `F2`, navigate to the `Plugins` page, and enable the plugin and features you wish to use.  

**_Delay value will need to be adjusted from F2 menu, I use 0.3 for private matches online, and 1.6 for LAN matches for HUD hiding function to work correctly. Adjust as necessary to work on your system_**

# RCON Commands

Declare an rconSend function in your frontend app, e.g.:

```js
const rconSend = (command) => {
  socket.emit('RCON', {
    data: {
      command: command,
    },
  });
}
```

example for a ButtonMash command:

```js
rconSend('ButtonMash 0');
```

Old RCON usage example for hiding HUD elements if you don't wish to use plugin:

```js
if (update.event === 'game:post_countdown_begin') {
  rconSend('rcon_refresh_allowed');
  rconSend('replay_gui hud 1');
  rconSend('replay_gui matchinfo 1');
  setTimeout(() => {
    rconSend('replay_gui hud 0');
    rconSend('replay_gui matchinfo 0');
  }, 100);
}
```